%define _enable_debug_packages %{nil}
%define debug_package          %{nil}
%define ver 15

Summary: Remote control and meeting solution
Name: teamviewer
Version: %{ver}.46.7
Release: 1
URL: https://www.teamviewer.com/en/products/teamviewer/
# https://www.teamviewer.com/en-us/download/linux/
Source0: https://dl.teamviewer.com/download/linux/version_%{ver}x/teamviewer_%{version}_amd64.tar.xz
Source1: https://static.teamviewer.com/resources/2023/01/FINAL_2023-01-11_EULA_EN.pdf
Source2: https://static.teamviewer.com/resources/2021/07/TeamViewer-Data-Processing-Agreement_EN.pdf
License: Proprietary
BuildRequires: chrpath
BuildRequires: desktop-file-utils
BuildRequires: sed
BuildRequires: systemd
Requires(post): %{_bindir}/mktemp
Requires(post): %{_sbindir}/semodule
Requires(postun): %{_sbindir}/semodule
Requires: dbus-common
Requires: hicolor-icon-theme
Requires: polkit
Requires: qt5-qtquickcontrols
ExclusiveArch: x86_64

%description
TeamViewer provides easy, fast and secure remote access and meeting solutions
to Linux, Windows PCs, Apple PCs and various other platforms,
including Android and iPhone.

TeamViewer is free for personal use.
You can use TeamViewer completely free of charge to access your private
computers or to help your friends with their computer problems.

To buy a license for commercial use, please visit http://www.teamviewer.com

%prep
%setup -q -n %{name}
# Change hard-coded paths in scripts
sed -i -e "s,/opt/teamviewer,%{_libdir}/%{name},g" \
  tv_bin/desktop/com.teamviewer.TeamViewer.desktop \
  tv_bin/script/com.teamviewer.TeamViewer{,.Desktop}.service \
  tv_bin/script/com.teamviewer.TeamViewer.policy \
  tv_bin/script/teamviewerd.RHEL.conf \
  tv_bin/script/teamviewerd.service \
  tv_bin/script/teamviewer_setup \
  tv_bin/script/tvw_*

# Switch operation mode from 'portable' to 'installed'
sed -i -e "s/TAR_NI/RPM/g" tv_bin/script/tvw_config

# unbundle Qt, ICU and xcb
rm -rv tv_bin/RTlib
chrpath -d tv_bin/{TeamViewer,teamviewer-config}

cp -p %{S:1} %{S:2} .

%build

%install
install -dm755 %{buildroot}{/etc/%{name},%{_bindir},/var/log/%{name}%{ver}}
install -dm755 %{buildroot}%{_unitdir}
install -dm755 %{buildroot}%{_libdir}/%{name}/tv_bin/{resources,script}
install -dm755 %{buildroot}%{_datadir}/dbus-1/{services,system.d}
install -dm755 %{buildroot}%{_datadir}/polkit-1/actions
for sz in 16 20 24 32 48 256 ; do
  install -Dpm644 tv_bin/desktop/%{name}_${sz}.png \
    %{buildroot}%{_datadir}/icons/hicolor/${sz}x${sz}/apps/TeamViewer.png
done
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin \
  tv_bin/{TeamViewer{,_Desktop},teamviewer{d,-config}}
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin/resources \
  tv_bin/resources/TVResource_*.so
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin/script \
  tv_bin/script/execscript
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin/script \
  tv_bin/script/teamviewer
desktop-file-install --vendor "" --dir %{buildroot}%{_datadir}/applications \
  tv_bin/desktop/com.teamviewer.TeamViewer.desktop
install -pm644 -t %{buildroot}%{_datadir}/dbus-1/services \
  tv_bin/script/com.teamviewer.TeamViewer{,.Desktop}.service
install -pm644 -t %{buildroot}%{_datadir}/dbus-1/system.d \
  tv_bin/script/com.teamviewer.TeamViewer.Daemon.conf
install -pm644 -t %{buildroot}%{_datadir}/polkit-1/actions \
  tv_bin/script/com.teamviewer.TeamViewer.policy
install -pm644 -t %{buildroot}%{_libdir}/%{name}/tv_bin/script \
  tv_bin/script/tvw_*
install -pm644 -t %{buildroot}%{_unitdir} \
  tv_bin/script/teamviewerd.service

ln -s %{_libdir}/%{name}/tv_bin/script/%{name} %{buildroot}%{_bindir}/%{name}
ln -s /etc/%{name} %{buildroot}%{_libdir}/%{name}/config
ln -s /var/log/%{name}%{ver} %{buildroot}%{_libdir}/%{name}/logfiles

# generate localized files list
find %{buildroot} -type f -o -type l|sed '
s:'"%{buildroot}"'::
s:\(.*/resources/TVResource_\)\([^/_]\+\)\(.*\.so$\):%lang(\2) \1\2\3:
s:^\([^%].*\)::
s:%lang(C) ::
s:zh:zh_:
/^$/d' > %{name}.lang

%preun
if [ $1 -eq 0 ]; then
  %{_libdir}/%{name}/tv_bin/teamviewerd --unmanage
fi
%systemd_preun teamviewerd.service

%post
TMPDIR=$(%{_bindir}/mktemp -d)
cat >> $TMPDIR/%{name}-rpm.cil << __EOF__
(typeattributeset cil_gen_require consolekit_t)
(typeattributeset cil_gen_require etc_t)
(typeattributeset cil_gen_require framebuf_device_t)
(typeattributeset cil_gen_require http_port_t)
(typeattributeset cil_gen_require init_t)
(typeattributeset cil_gen_require lib_t)
(typeattributeset cil_gen_require NetworkManager_t)
(typeattributeset cil_gen_require systemd_logind_t)
(typeattributeset cil_gen_require unconfined_dbusd_t)
(typeattributeset cil_gen_require unconfined_t)
(typeattributeset cil_gen_require var_log_t)
(typeattributeset cil_gen_require vnc_port_t)
(typeattributeset cil_gen_require xserver_t)
(allow consolekit_t init_t (dbus (send_msg)))
(allow init_t etc_t(file (append unlink)))
(allow init_t framebuf_device_t(chr_file (ioctl open read write)))
(allow init_t http_port_t(tcp_socket (name_connect)))
(allow init_t lib_t(dir (add_name)))
(allow init_t lib_t(file (create write)))
(allow init_t NetworkManager_t(netlink_generic_socket (getattr)))
(allow init_t self(process (ptrace)))
(allow init_t systemd_logind_t(netlink_kobject_uevent_socket (getattr)))
(allow init_t unconfined_dbusd_t(unix_stream_socket (getattr)))
(allow init_t unconfined_t(netlink_route_socket (getattr)))
(allow init_t unconfined_t(tcp_socket (getattr)))
(allow init_t unconfined_t(unix_stream_socket (getattr)))
(allow init_t var_log_t(file (create)))
(allow init_t vnc_port_t(tcp_socket (name_connect)))
(allow init_t xserver_t(unix_stream_socket (connectto)))
__EOF__
%{_sbindir}/semodule -i $TMPDIR/%{name}-rpm.cil
rm -rf $TMPDIR
%systemd_post teamviewerd.service

%postun
%systemd_postun_with_restart teamviewerd.service
if [ $1 -eq 0 ]; then
  %{_sbindir}/semodule -r %{name}-rpm > /dev/null || :
fi

%files -f %{name}.lang
%license FINAL_2023-01-11_EULA_EN.pdf
%doc TeamViewer-Data-Processing-Agreement_EN.pdf
%dir /etc/%{name}
%attr(0644,root,root) %config(noreplace) %ghost /etc/%{name}/global.conf
%{_bindir}/%{name}
%{_unitdir}/teamviewerd.service
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/config
%{_libdir}/%{name}/logfiles
%dir %{_libdir}/%{name}/tv_bin
%dir %{_libdir}/%{name}/tv_bin/script
%{_libdir}/%{name}/tv_bin/script/execscript
%{_libdir}/%{name}/tv_bin/script/%{name}
%{_libdir}/%{name}/tv_bin/script/tvw_*
%{_libdir}/%{name}/tv_bin/TeamViewer
%{_libdir}/%{name}/tv_bin/TeamViewer_Desktop
%{_libdir}/%{name}/tv_bin/teamviewer-config
%{_libdir}/%{name}/tv_bin/teamviewerd
%{_datadir}/applications/com.teamviewer.TeamViewer.desktop
%{_datadir}/dbus-1/services/com.teamviewer.TeamViewer.Desktop.service
%{_datadir}/dbus-1/services/com.teamviewer.TeamViewer.service
%{_datadir}/dbus-1/system.d/com.teamviewer.TeamViewer.Daemon.conf
%{_datadir}/icons/hicolor/*/apps/TeamViewer.png
%{_datadir}/polkit-1/actions/com.teamviewer.TeamViewer.policy
/var/log/%{name}%{ver}

%changelog
* Thu Oct 19 2023 Dominik Mierzejewski <dominik@greysector.net> 15.46.7-1
- update to 15.46.7

* Sun Jun 11 2023 Dominik Mierzejewski <dominik@greysector.net> 15.42.4-1
- update to 15.42.4

* Sun May 01 2022 Dominik Mierzejewski <rpm@greysector.net> 15.29.4-1
- update to 15.29.4

* Fri Feb 11 2022 Dominik Mierzejewski <rpm@greysector.net> 15.25.5-1
- update to 15.25.5

* Wed Oct 06 2021 Dominik Mierzejewski <rpm@greysector.net> 15.22.3-1
- update to 15.22.3

* Sun Aug 01 2021 Dominik Mierzejewski <rpm@greysector.net> 15.20.3-1
- update to 15.20.3
- unbundle included Qt, ICU and xcb

* Mon May 24 2021 Dominik Mierzejewski <rpm@greysector.net> 15.17.6-1
- update to 15.17.6
- fix typo in postun script

* Fri Mar 19 2021 Dominik Mierzejewski <rpm@greysector.net> 15.15.5-1
- update to 15.15.5
- include missing D-BUS config
- set installation type to RPM instead of TAR
- update SELinux policy

* Wed Jan 20 2021 Dominik Mierzejewski <rpm@greysector.net> 15.13.6-1
- update to 15.13.6
- include execscript
- fix path in polkit policy

* Mon Jul 20 2020 Dominik Mierzejewski <rpm@greysector.net> 15.7.6-1
- update to 15.7.6

* Fri Jan 03 2020 Dominik Mierzejewski <rpm@greysector.net> 15.1.3937-1
- update to 15.1.3937
- update SELinux policy
- fix typo in systemd scriptlet

* Sat Nov 09 2019 Dominik Mierzejewski <rpm@greysector.net> 14.7.1965-1
- update to 14.7.1965

* Fri Oct 04 2019 Dominik Mierzejewski <rpm@greysector.net> 14.6.2452-2
- fix build with coreutils < 8.23 (e.g. RHEL7)
- keep arch in tarball file name (Tru Huynh)

* Fri Oct 04 2019 Dominik Mierzejewski <rpm@greysector.net> 14.6.2452-1
- update to 14.6.2452
- make downloaded tarball versioned

* Sat Aug 24 2019 Dominik Mierzejewski <rpm@greysector.net> 14.5.1691-1
- update to 14.5.1691

* Mon Jul 22 2019 Dominik Mierzejewski <rpm@greysector.net> 14.4.2669-1
- update to 14.4.2669

* Fri Apr 12 2019 Dominik Mierzejewski <rpm@greysector.net> 14.2.2558-1
- update to 14.2.2558
- fix locations of icons

* Wed Jan 02 2019 Dominik Mierzejewski <rpm@greysector.net> 14.1.3399-1
- update to 14.1.3399

* Mon Nov 19 2018 Dominik Mierzejewski <rpm@greysector.net> 14.0.12762-1
- initial build
